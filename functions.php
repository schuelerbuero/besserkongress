<?php
add_theme_support( 'post-thumbnails' );

register_nav_menus( array(
	'main-menu' => 'Hauptmenü',
	'footer-menu' => 'Footermenü',
));
