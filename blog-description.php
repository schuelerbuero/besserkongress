<?php
  /**
   * This file fetches a page called besser::blog from the database. Make sure it exists.
   */
  $page = get_page_by_title('besser::blog');
?>
<div class="teaser">
  <div class="content">
    <h1>besser::blog</h1>
    <hr class="content-underline">
    <br><br>
    <?php
     echo $page->post_content;
    ?>
  </div>
</div>
