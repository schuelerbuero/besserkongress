<?php get_header(); ?>

<div class="content">
  <?php $category_link = get_category_link(get_cat_ID('Blog')); ?>
  <a href="<?php echo $category_link;?>?ref=article" class="content-backlink">besser::blog</a>
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <h1 class="content-headline"><?php the_title(); ?></h1>
    <?php the_post_thumbnail('post-thumbnail', array('class' => 'content-leading-thumbnail')); ?>

    <div class="content-meta">
      <?php
        $author = get_post_meta($post->ID, 'author', true);
        if($author != false){
          echo "<div class='content-meta-author'>

          Von " . $author . "</div>";
        }
      ?>
      <div class="content-meta-date">
        <?php the_date('j. F Y - G:i');?>
      </div>
    </div>
    <hr class="content-underline"><br>

    <div class="content-content">

      <p>
        <?php the_content(); ?>
      </p>
    </div>
  <?php endwhile; endif; ?>
  <?php include('sharingbar.php'); ?>
  <a href="<?php echo $category_link;?>?ref=article" class="button button-outline">zurück zur Übersicht</a>
</div>


<?php get_footer(); ?>
