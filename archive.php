<?php
/**
 * Unless a developer includes meta data with permalinks in their templates, the archive.php will not be used.
 * Meta data is information tied to the post. For example the date something was posted on, the author, and any categories, tags, or taxonomies used for the post are all examples of meta data.
 * When a visitor to a website clicks on the meta data, the archive.php will render any posts associated with that piece of meta data. 
 * For example, if a visitor clicks on the name of an author, the archive.php will display all posts by that author.
 */
