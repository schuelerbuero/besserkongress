#Wordpress Theme besserkongress
Dieses Repostory enthält das Wordpress Theme für die Veranstaltung [besser::](besser-kongress.de) vom [Münchner Schülerbüro e.V.](schuelerbuero.de).

## Sass / SCSS
Dieses Repository verwendet die CSS-Erweiterung Sass. Um die Sass-Dateien in CSS zu kompilieren kann, wenn sass installiert ist, das folgende Kommando verwendet werden:

 	sass --watch sass/style.scss:style.css


## Mitwirken
Um an diesem Repository mitzuwirken sollten die folgenden Hinweise beachtet werden:

* Der Code und alle Kommentare werden auf Englisch verfasst.
* Für Variablen wird die [CamelCase schreibweise](https://en.wikipedia.org/wiki/CamelCase) verwendet.

### Branching
Für jede Aufgabe, die eine Änderung an der Codebasis erfordert wird ein eigener Branch angelegt. Dies gilt sowohl für feature- als auch für bugfix-Branches. Die Branches werden immer vom master-Branch abgeleitet und in diesen zurückgeführt. Ein Mergen ist aus Gründen der Dokumentation nur mit Pull-Requst erlaubt.-
