<div class="sharingbar">
  <h3>Diesen Artikel teilen</h3>
  <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="blank"><img src="<?php bloginfo('template_directory'); ?>/ressources/img/icons/facebook_blue.svg" class="footer-social-icon"></a>
  <a href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>&text=<?php the_title ?>&hashtags=besserkongress%20via%20%40schuelerbuero" target="blank"><img src="<?php bloginfo('template_directory'); ?>/ressources/img/icons/twitter_blue.svg" class="footer-social-icon"></a>
</div>
