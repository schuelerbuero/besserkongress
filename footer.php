  </main>
  <footer>
    <div class="footer-inner">
      <p class="footer-text-primary">Münchner Schüler*innenbüro e.V.</p>
      <p class="footer-text-address">
        Haus der Jugendarbeit <br>
        Rupprechtstraße 29<br>
        80636 München <br>
        <br>
        E-Mail: <a href="mailto:info@schuelerinnenbuero.de" class="footer-link">info@schuelerinnenbuero.de</a><br>
        <a href="https://www.schuelerinnenbuero.de" target="blank" class="footer-link">www.schuelerinnenbuero.de</a>

      </p>
      <div class="footer-social">
        <a href="https://facebook.com/besserkongress" target="blank"><img src="<?php bloginfo('template_directory'); ?>/ressources/img/icons/facebook.svg" class="footer-social-icon"></a>
        <a href="https://instagram.com/schuelerbuero" target="blank"><img src="<?php bloginfo('template_directory'); ?>/ressources/img/icons/instagram.svg" class="footer-social-icon"></a>
        <!-- <a href="https://twitter.com/schuelerbuero" target="blank"><img src="<?php bloginfo('template_directory'); ?>/ressources/img/icons/twitter.svg" class="footer-social-icon"></a> -->
        <a href="https://youtube.com/schuelerbuero" target="blank"><img src="<?php bloginfo('template_directory'); ?>/ressources/img/icons/youtube.svg" class="footer-social-icon"></a>
      </div>
      <div class="footer-navigation">
        <?php
          wp_nav_menu( array(
            'theme_location' => 'footer-menu',
            'container_class'=>'footer-navigation-menu',
            'container' => 'div',
            'items_wrap' => ' <ul>%3$s</ul>',
          ));
        ?>
      </div>
    </div>
  <?php wp_footer(); ?>
  </footer>
 </body>
</html>
