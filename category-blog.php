<?php get_header(); ?>
<?php include('blog-description.php'); ?>
  <?php $i = 0; ?>
  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class="teaser <?php if($i % 2 == 0) echo 'teaser-dark'; ?>">
      <div class="content">
        <!-- print the headline besser::blog if it is the first article -->

      <a href="<?php the_permalink(); ?>" class="teaser-link">
        <?php the_post_thumbnail('post-thumbnail'); ?>
        <h2><?php the_title();?></h2>
      </a>
      <div class="teaser-meta">
        <?php
          $author = get_post_meta($post->ID, 'author', true);
          if($author != false){
            echo "<div class='teaser-meta-author'>

            Von " . $author . "</div>";
          }
        ?>
        <div class="teaser-meta-date">
          <?php the_date('j. F Y - G:i');?>          
        </div>

      </div>
      <div class="teaser-text">
        <p>
          <?php the_excerpt('Zum Artikel', TRUE); ?>
        </p>
        <a href="<?php the_permalink(); ?>">Zum Artikel</a>
      </div><!-- /teaser-text -->
      </div>

    </div><!-- /teaser -->
    <?php $i++ ?>
  <?php endwhile; else : ?>
  	<p><?php _e( 'Es wurden leider keine Beiträge gefunden.' ); ?></p>
  <?php endif; ?>

<?php get_footer(); ?>
