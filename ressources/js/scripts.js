$(document).ready(function documentReady(){
  var headerNavigationMenu = $('.header-navigation-menu');
  var headerNavigationBurger = $('.header-navigation-burger');

  headerNavigationBurger.click(function toggleMenuVisibility(){
    headerNavigationMenu.toggleClass('is-visible');
    headerNavigationBurger.toggleClass('header-navigation-burger--clicked');
  });
});
