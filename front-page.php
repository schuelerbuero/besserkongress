<?php
/**
 * This file is responsible for rendering the detailed page
 */
get_header(); ?>

<div class="content">
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <h1 class="content-headline"><?php the_title(); ?></h1>
    <hr class="content-underline"><br>
    <div class="content-content">
      <p>
        <?php the_content(); ?>
      </p>
    </div>
  <?php endwhile; endif; ?>
</div>

<?php get_footer(); ?>
